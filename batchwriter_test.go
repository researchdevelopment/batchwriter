package batchwriter

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func Test_service_Add(t *testing.T) {
	type fields struct {
		batchSize   int
		batchPeriod time.Duration
		writer      Writer
		input       chan ElementType
		buffer      []ElementType
		inShutdown  atomicBool
		close       chan struct{}
		flush       bool
		flushed     time.Time
	}
	type args struct {
		ctx  context.Context
		data []ElementType
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantErr   bool
		wantError error
	}{
		{
			name: "Service shutdown",
			fields: fields{
				inShutdown: atomicBool(1),
			},
			args: args{
				ctx:  context.Background(),
				data: []ElementType{1, 2, 3},
			},
			wantErr:   true,
			wantError: ErrServiceClosed,
		}, {
			name: "Service running",
			fields: fields{
				inShutdown: atomicBool(0),
			},
			args: args{
				ctx:  context.Background(),
				data: []ElementType{1, 2, 3},
			},
			wantErr:   false,
			wantError: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service{
				batchSize:   tt.fields.batchSize,
				batchPeriod: tt.fields.batchPeriod,
				writer:      tt.fields.writer,
				input:       tt.fields.input,
				buffer:      tt.fields.buffer,
				inShutdown:  tt.fields.inShutdown,
				close:       tt.fields.close,
				flush:       tt.fields.flush,
				flushed:     tt.fields.flushed,
			}
			s.Serve(tt.args.ctx)
			err := s.Add(tt.args.ctx, tt.args.data...)
			assert.Error(t, tt.wantError, err)
		})
	}
}
