package main

import (
	"batchwriter"
	"context"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

var wr = func(el ...batchwriter.ElementType) error {
	log.Printf("%#v", el)
	time.Sleep(1 * time.Second)
	return nil
}

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer stop()

	service := batchwriter.New(wr,
		batchwriter.WithBatchSize(2),
		batchwriter.WithBatchPeriod(2*time.Second),
	)

	for i := 0; i < 3; i++ {
		go func(ctx context.Context) {
			for {
				size := rand.Intn(5)
				data := make([]batchwriter.ElementType, size)
				for i := 0; i < size; i++ {
					data[i] = rand.Intn(1000)
				}

				service.Add(ctx, data...)
				time.Sleep(time.Duration(rand.Intn(1000))*time.Millisecond + 500*time.Millisecond)
			}
		}(ctx)
	}

	go func() {
		<-ctx.Done()
		//ctx, cancel := context.WithBatchPeriod(context.Background(), 3*time.Second)
		//defer cancel()
		service.Shutdown(ctx)
	}()
	log.Fatal(service.Serve(ctx))
}
