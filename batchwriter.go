package batchwriter

import (
	"context"
	"log"
	"sync/atomic"
	"time"
)

type atomicBool int32

func (b *atomicBool) isSet() bool { return atomic.LoadInt32((*int32)(b)) != 0 }
func (b *atomicBool) setTrue()    { atomic.StoreInt32((*int32)(b), 1) }
func (b *atomicBool) setFalse()   { atomic.StoreInt32((*int32)(b), 0) }

type (
	// ElementType - это алиас, а не сам тип.
	// Мне бы хотелось, чтобы в полученном исходнике можно было заменить его на любой другой тип
	// и всё продолжало работать (через несколько месяцев мы сможем написать Service[ElementType any],
	// но суть от этого никак поменяться не должна).
	ElementType = interface{} // тип обрабатываемых данных

	Writer = func(...ElementType) error // писатель в базу

	Service interface {
		Serve(ctx context.Context) error                    // запуск сервиса
		Add(ctx context.Context, data ...ElementType) error // передача данных
		Shutdown(context.Context) error                     // остановка сервиса
	}

	service struct {
		batchSize   int
		batchPeriod time.Duration
		writer      Writer

		input  chan ElementType
		buffer []ElementType

		inShutdown atomicBool // true when server is in shutdown
		close      chan struct{}

		flush   bool
		flushed time.Time
	}
)

var (
	_ Service = (*service)(nil)
)

func New(writer Writer, options ...Option) Service {

	svc := &service{
		writer: writer,
		input:  make(chan ElementType, 1<<20),
	}

	// Apply all options
	for _, opt := range options {
		opt(svc)
	}

	// default options
	if svc.batchSize <= 0 {
		svc.batchSize = defaultBatchSize
	}
	if svc.batchPeriod <= 0 {
		svc.batchPeriod = defaultBatchPeriod
	}

	svc.buffer = make([]ElementType, 0, svc.batchSize)

	return svc
}

func (s *service) Serve(ctx context.Context) error {
	log.Println("Service running...")
	defer log.Println("Service stopped")

	timer := time.NewTimer(s.batchPeriod)

	for {
		if s.flush {
			if err := s.write(); err != nil {
				return err
			}
			s.flush = false
			s.flushed = time.Now()
			timer = time.NewTimer(s.batchPeriod)
		}

		select {
		case <-ctx.Done():
			if err := s.forceWrite(); err != nil {
				return err
			}
			return ctx.Err()
		case <-s.close:
			return s.forceWrite()
		case <-timer.C:
			s.flush = true
		case data := <-s.input:
			s.buffer = append(s.buffer, data)
			if len(s.buffer) > s.batchSize {
				s.flush = true
			}

		}
	}

	return nil
}

func (s *service) Add(_ context.Context, data ...ElementType) error {
	if s.inShutdown.isSet() {
		return ErrServiceClosed
	}
	for i := 0; i < len(data); i++ {
		s.input <- data[i]
	}
	return nil
}

func (s *service) Shutdown(_ context.Context) error {
	if s.inShutdown.isSet() {
		return ErrServiceClosed
	}

	s.shutdown()
	s.close <- struct{}{}

	return nil
}

func (s *service) shutdown() {
	s.inShutdown.setTrue()
	log.Println("Service stopping..")
}

func (s *service) forceWrite() error {
	for len(s.input) > 0 {
		data := <-s.input
		s.buffer = append(s.buffer, data)
	}

	return s.write()
}

func (s *service) write() error {
	if len(s.buffer) == 0 {
		return nil
	}

	if err := s.writer(s.buffer...); err != nil {
		return err
	}
	s.buffer = make([]ElementType, 0, s.batchSize)

	return nil
}
