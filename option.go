package batchwriter

import "time"

const (
	defaultBatchSize   = 10
	defaultBatchPeriod = 1 * time.Second
)

type Option func(*service)

// N - ограничение на количество
func WithBatchSize(n int) Option {
	return func(s *service) {
		s.batchSize = n
	}
}

// X - ограничение на таймаут
func WithBatchPeriod(t time.Duration) Option {
	return func(s *service) {
		s.batchPeriod = t
	}
}
