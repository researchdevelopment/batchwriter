package batchwriter

import "errors"

var ErrServiceClosed = errors.New("Service closed")
